#!/usr/bin/env python3
import subprocess
import sys
import os

def help():
  print("Parameter 1: MIN/MAX/INC/DEC/OFF");
  sys.exit()

if len(sys.argv) != 2:
  help()
 
scriptPath = os.path.dirname(os.path.realpath(__file__))

start = "1010111001001000"
end = "000001000000"

# Power controll
if sys.argv[1] == "MIN":
   mode = "0010"
elif sys.argv[1] == "INC":
   mode = "0100"
elif sys.argv[1] == "DEC":
  mode = "1100"
elif sys.argv[1] == "MAX":
  mode = "1000"
elif sys.argv[1] == "OFF":
  mode = "1010"
elif sys.argv[1] == "30":
  mode = "0110"
elif sys.argv[1] == "60":
  mode = "0001"
else:
  help()   
command = start + mode + end

subprocess.call([scriptPath+"/send-stuff-light",command])
print(command)
