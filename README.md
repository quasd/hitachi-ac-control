# hitachi-ac-control

AC used in this project is Hitachi RAS-AJ56H2 (W)
For sending the commands I am using raspberry 4 with [ir shield](http://www.raspberrypiwiki.com/index.php/Raspberry_Pi_IR_Control_Expansion_Board) if you are using something else you will have to adjust the used pins in record.py and send-signal.c

## Dependencies

### pigpio

```
git clone https://github.com/joan2937/pigpio
cd pigpio
vim pigpio.h
   #define PI_WAVE_BLOCKS 100  <--- increase to allow longer commands
make
sudo make install
```

### irslinger.h

```
wget https://raw.githubusercontent.com/bschwind/ir-slinger/master/irslinger.h

```

make following modifiations

My hitachi ac seems to be using 2 leading pulses, to allow this make following changes and increase the maximum command and pulse size
```
diff --git a/irslinger.h b/irslinger.h
index 84d8f1b..ec847bb 100644
--- a/irslinger.h
+++ b/irslinger.h
@@ -5,8 +5,8 @@
 #include <math.h>
 #include <pigpio.h>
 
-#define MAX_COMMAND_SIZE 512
-#define MAX_PULSES 12000
+#define MAX_COMMAND_SIZE 2092
+#define MAX_PULSES 24000
 
 static inline void addPulse(uint32_t onPins, uint32_t offPins, uint32_t duration, gpioPulse_t *irSignal, int *pulseCount)
 {
@@ -57,13 +57,15 @@ static inline int irSling(uint32_t outPin,
        double dutyCycle,
        int leadingPulseDuration,
        int leadingGapDuration,
+       int leadingPulseDurationTwo,
+       int leadingGapDurationTwo,
        int onePulse,
        int zeroPulse,
        int oneGap,
        int zeroGap,
        int sendTrailingPulse,
        const char *code)
-{
+{ 
        if (outPin > 31)
        {
                // Invalid pin number
@@ -87,6 +89,9 @@ static inline int irSling(uint32_t outPin,
        carrierFrequency(outPin, frequency, dutyCycle, leadingPulseDuration, irSignal, &pulseCount);
        gap(outPin, leadingGapDuration, irSignal, &pulseCount);
 
+        carrierFrequency(outPin, frequency, dutyCycle, leadingPulseDurationTwo, irSignal, &pulseCount);
+        gap(outPin, leadingGapDurationTwo, irSignal, &pulseCount);
+
        int i;
        for (i = 0; i < codeLen; i++)
        {
```

## Install

```
git clone https://gitlab.com/quasd/hitachi-ac-control
cd hitachi-ac-control
cp <path to irslinger.h> ./
gcc -o send-stuff send-signal.c -lm -lpigpio -pthread -lrt
```

## Usage 

Now you should be able to record commands with record.py and send commands with send-stuff <binary command>
* If you are using different AC than I am using, you might have to adjust following variables in send-signal.c
To find correct values you can use record.py 

```
	int leadingPulseDuration = 29731; // The duration of the beginning pulse in microseconds
	int leadingGapDuration = 49275;   // The duration of the gap in microseconds after the leading pulse
        int leadingPulseDurationTwo = 3415;
        int leadingGapDurationTwo = 1605;
	int onePulse = 470;              // The duration of a pulse in microseconds when sending a logical 1
	int zeroPulse = 470;             // The duration of a pulse in microseconds when sending a logical 0
	int oneGap = 1205;               // The duration of the gap in microseconds when sending a logical 1
	int zeroGap = 367;               // The duration of the gap in microseconds when sending a logical 0

```

The lead pulses/gap.

```
root@rasp4:~/hitachi-ac-control# ./record.py 
----------Start----------
0 29726   <---- leadingPulseDuration
1 49274   <---- leadingGapDuration
0 3416    <---- leadingPulseDurationTwo
1 1604    <---- leadingGapDurationTwo
```

logical 1 pulse
```
0 469     <---- onePulse
1 1206    <---- oneGap
```

logical 0 pulse
```
0 469     <---- zeroPulse
1 368     <---- zeroGap
```

### record.py

```
./record.py
.
.
.
-----------End-----------

Size of array is 853
111000000000001000000000000000001011111101111111110000000000110011110011000100100110110110110010000011011100000110111110010000000011111111000000001111111100000000111111110000000011111111000000001111111111001010001101011000111101110000000000001111111100000000111111110000000111111110110000000011111110000000011111110001000111101110000000001111111100000000111111111111111100000000111111110000000011111111000000001111111100000000
0x380080002fdff0033cc49b6c83706f900ff00ff00ff00ff00ffca358f7000ff00ff01fec03f807f11ee00ff00ffff00ff00ff00ff00L
```

The binary in the output also includes the leading pulses. To get the real binary remove the first two characters.

Result

```
1000000000001000000000000000001011111101111111110000000000110011110011000100100110110110110010000011011100000110111110010000000011111111000000001111111100000000111111110000000011111111000000001111111111001010001101011000111101110000000000001111111100000000111111110000000111111110110000000011111110000000011111110001000111101110000000001111111100000000111111111111111100000000111111110000000011111111000000001111111100000000
```

### send-stuff

Use a camera to confirm, the ir-lend is sending anything. 
```
root@rasp4:~/hitachi-ac-control# ./send-stuff 1000000000001000000000000000001011111101111111110000000000110011110011000100100110110110110010000011011100000110111110010000000011111111000000001111111100000000111111110000000011111111000000001111111111001010001101011000111101110000000000001111111100000000111111110000000111111110110000000011111110000000011111110001000111101110000000001111111100000000111111111111111100000000111111110000000011111111000000001111111100000000
code size is 424
pulse count is 18246
Result: 36067
```

### ac.py

```
root@rasp4:~/hitachi-ac-control# ./ac.py 
Parameter 1: temp, Parameter 2: cool/warm/dry, Parameter 3: on/off
root@rasp4:~/hitachi-ac-control# ./ac.py 24 cool on
000110
Temp:000110
inverse:11111001
ASD
code size is 424
pulse count is 18246
Result: 36067
root@rasp4:~/hitachi-ac-control#
```

# TODO
- Document binary bits
- Air speed control
- Air direction control
