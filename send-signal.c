#include <stdio.h>
#include "irslinger.h"

int main(int argc, char *argv[])
{   
       // printf("Parameters %d\n",argc);
        if( argc == 2 ) {
            ; //printf("jee");
        } else {
            printf("Give command in binary as parameter\n");
            return 0;
        }
        //printf("Command is :%s\n",argv[1]);
	uint32_t outPin = 17;            // The Broadcom pin number the signal will be sent on
	int frequency = 38000;           // The frequency of the IR signal in Hz
	double dutyCycle = 0.5;          // The duty cycle of the IR signal. 0.5 means for every cycle,
	                                 // the LED will turn on for half the cycle time, and off the other half
	int leadingPulseDuration = 29731; // The duration of the beginning pulse in microseconds
	int leadingGapDuration = 49275;   // The duration of the gap in microseconds after the leading pulse
        int leadingPulseDurationTwo = 3415;
        int leadingGapDurationTwo = 1605;
	int onePulse = 470;              // The duration of a pulse in microseconds when sending a logical 1
	int zeroPulse = 470;             // The duration of a pulse in microseconds when sending a logical 0
	int oneGap = 1205;               // The duration of the gap in microseconds when sending a logical 1
	int zeroGap = 367;               // The duration of the gap in microseconds when sending a logical 0
	int sendTrailingPulse = 1;       // 1 = Send a trailing pulse with duration equal to "onePulse"
	                                 // 0 = Don't send a trailing pulse
	int result = irSling(
		outPin,
		frequency,
		dutyCycle,
		leadingPulseDuration,
		leadingGapDuration,
                leadingPulseDurationTwo,
                leadingGapDurationTwo,
		onePulse,
		zeroPulse,
		oneGap,
		zeroGap,
		sendTrailingPulse,
                argv[1]);
	
	return result;
}
