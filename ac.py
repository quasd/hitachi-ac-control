#!/usr/bin/env python3
import subprocess
import sys
import os

scriptPath = os.path.dirname(os.path.realpath(__file__))
temperature = 24
command = "10000000000010000000000000000010111111011111111100000000001100111100110001001001101101101"


if len(sys.argv) != 5:
  print("Parameter 1: temp, Parameter 2: cool/warm/dry, Parameter 3: on/off, Parameter 4: move on/off");
  sys.exit()

if sys.argv[4] == "on":
   command += "00000010111111000"
elif sys.argv[4] == "off":
   command += "10010000011011100"


temp = sys.argv[1]
tempBinary = str(format(int(temp), '06b'))[::-1]
print(tempBinary)
tempBinaryInverse = ''.join('1' if x == '0' else '0' for x in "00" + tempBinary)
command += tempBinary + tempBinaryInverse
print ("Temp:" + tempBinary)
print ("inverse:" + tempBinaryInverse)

beforeMode = "00000000111111110000000011111111000000001111111100000000111111110000000011111111"
command += beforeMode

if sys.argv[2] == "cool":
   mode = "11001010001"
elif sys.argv[2] == "warm":
   mode = "01101010100"
elif sys.argv[2] == "dry":
   mode = "10101010010"

command += mode

afterMode = "101011000"
command += afterMode

if sys.argv[3] == "on":
   command += "1"
elif sys.argv[3] == "off":
   command += "0"
   

command +=    "1110111"

if sys.argv[3] == "on":
   command += "0"
elif sys.argv[3] == "off":
   command += "1"

end = "000000000001111111100000000111111110000000111111110110000000011111110000000011111110001000111101110000000001111111100000000111111111111111100000000111111110000000011111111000000001111111100000000"
command += end

subprocess.call([scriptPath+"/send-stuff",command])
print(command)
